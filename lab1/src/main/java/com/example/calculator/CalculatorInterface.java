package com.example.calculator;

public interface CalculatorInterface {
    double calculate(double left, double right, String operation);

    void readCommands();

    void performCalculation(double left, double right, String op);
}
