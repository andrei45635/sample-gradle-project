package com.example.calculator;

import com.example.factory.OperationFactory;
import com.example.operations.Operation;

import java.util.Scanner;

public class Calculator implements CalculatorInterface {
    private final OperationFactory factory = new OperationFactory();

    public double calculate(double left, double right, String operation) {
        try {
            Operation operationValue = factory.createOperation(operation);
            return operationValue.calculate(left, right);
        } catch (IllegalArgumentException iae) {
            System.out.println(iae.getMessage());
        }
        return Double.NaN;
    }

    public void performCalculation(double left, double right, String operation) {
        try {
            double result = calculate(left, right, operation);
            System.out.println("Result: " + result);
        } catch (ArithmeticException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public void readCommands() {
        Scanner scanner = new Scanner(System.in);
        Calculator calculator = new Calculator();

        while (true) {
            System.out.print("Enter command: ");
            String input = scanner.nextLine();

            String[] tokens = input.split(" ");

            if (tokens.length > 0 && tokens[0].equalsIgnoreCase("exit")) {
                System.out.println("Exiting the calculator program.");
                break;
            }

            String operation;
            double left;
            double right;

            operation = tokens[0].toLowerCase();
            if (operation.equals("sqrt")) {
                if (tokens.length != 2) {
                    System.out.println("Please input only one operand.");
                    continue;
                }

                left = Double.parseDouble(tokens[1]);
                try {
                    double res = calculator.calculate(left, 0, operation);
                    System.out.println("Result: " + res);
                } catch (ArithmeticException e) {
                    System.out.println("Error: " + e.getMessage());
                }
            } else {
                if (tokens.length < 3) {
                    System.out.println("Invalid input.");
                    continue;
                }
                try {
                    left = Double.parseDouble(tokens[1]);
                    right = Double.parseDouble(tokens[2]);
                } catch (NumberFormatException nfe) {
                    System.out.println("Error: Invalid input.");
                    continue;
                }
                performCalculation(left, right, operation);
            }
        }
    }
}
