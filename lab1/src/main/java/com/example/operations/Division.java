package com.example.operations;

public class Division implements Operation{
    @Override
    public double calculate(double leftOperand, double rightOperand) {
        if(rightOperand == 0){
            throw new ArithmeticException("Division by zero");
        }
        return leftOperand / rightOperand;
    }
}
