package com.example.operations;

public class Addition implements Operation{
    @Override
    public double calculate(double leftOperand, double rightOperand) {
        return leftOperand + rightOperand;
    }
}
