package com.example.operations;

public class SquareRoot implements Operation{
    @Override
    public double calculate(double leftOperand, double rightOperand) {
        if(leftOperand < 0 || rightOperand < 0){
            throw new ArithmeticException("Square root of a negative number");
        }
        return Math.sqrt(leftOperand);
    }
}
