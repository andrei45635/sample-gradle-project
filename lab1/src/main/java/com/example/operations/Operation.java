package com.example.operations;

public interface Operation {
    double calculate(double leftOperand, double rightOperand);
}
