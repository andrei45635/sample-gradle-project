package com.example.operations;

public class MaxOperation implements Operation{
    @Override
    public double calculate(double leftOperand, double rightOperand) {
        return Math.max(leftOperand, rightOperand);
    }
}
