package com.example.operations;

public class MinOperation implements Operation{
    @Override
    public double calculate(double leftOperand, double rightOperand) {
        return Math.min(leftOperand, rightOperand);
    }
}
