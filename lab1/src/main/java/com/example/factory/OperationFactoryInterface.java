package com.example.factory;

import com.example.operations.Operation;

public interface OperationFactoryInterface {
    Operation createOperation(String operationType);
}
