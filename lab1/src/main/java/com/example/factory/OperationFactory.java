package com.example.factory;

import com.example.operations.Addition;
import com.example.operations.Subtraction;
import com.example.operations.Multiplication;
import com.example.operations.Division;
import com.example.operations.MinOperation;
import com.example.operations.MaxOperation;
import com.example.operations.SquareRoot;
import com.example.operations.Operation;

public class OperationFactory implements OperationFactoryInterface{
    public Operation createOperation(String operationType) {
        switch (operationType) {
            case "add":
                return new Addition();
            case "sub":
                return new Subtraction();
            case "mul":
                return new Multiplication();
            case "div":
                return new Division();
            case "min":
                return new MinOperation();
            case "max":
                return new MaxOperation();
            case "sqrt":
                return new SquareRoot();
            default:
                throw new IllegalArgumentException("Invalid operation!");
        }
    }
}
