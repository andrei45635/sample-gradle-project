package test;

import com.example.calculator.Calculator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {
    private final Calculator calculator = new Calculator();

    @Test
    void addsTwoNumbers() {
        System.out.println("Testing addition ('add 6 2')...");
        assertEquals(8.0, calculator.calculate(6.0, 2.0, "add"), 0.001);
        System.out.println("Passed the addition test...");
    }

    private void vdc(String str1, String str2, String operation) {
        double number1 = Double.parseDouble(str1);
        double number2 = Double.parseDouble(str2);
        calculator.calculate(number1, number2, operation);
    }

    @Test
    void addsTwoInvalidNumbers() {
        System.out.println("Testing addition ('add t b')...");
        assertThrows(NumberFormatException.class, () -> vdc("t", "b", "add"));
        System.out.println("Passed the addition test...");
    }

    @Test
    void subtractsTwoNumbers() {
        System.out.println("Testing subtraction ('sub 6 2')...");
        assertEquals(4.0, calculator.calculate(6.0, 2.0, "sub"), 0.001);
        System.out.println("Passed the subtraction test...");
    }

    @Test
    void subtractsTwoInvalidNumbers() {
        System.out.println("Testing subtraction ('sub t b')...");
        assertThrows(NumberFormatException.class, () -> vdc("t", "b", "sub"));
        System.out.println("Passed the subtraction test...");
    }

    @Test
    void multipliesTwoNumbers() {
        System.out.println("Testing multiplication ('mul 6 2')...");
        assertEquals(12.0, calculator.calculate(6.0, 2.0, "mul"), 0.001);
        System.out.println("Passed the multiplication test...");
    }

    @Test
    void multipliesTwoInvalidNumbers() {
        System.out.println("Testing multiplication ('mul t b')...");
        assertThrows(NumberFormatException.class, () -> vdc("t", "b", "mul"));
        System.out.println("Passed the multiplication test...");
    }

    @Test
    void dividesTwoNumbers() {
        System.out.println("Testing division ('div 6 2')...");
        assertEquals(3.0, calculator.calculate(6.0, 2.0, "div"), 0.001);
        System.out.println("Passed the division test...");
    }

    @Test
    void dividesTwoInvalidNumbers() {
        System.out.println("Testing division ('div t b')...");
        assertThrows(NumberFormatException.class, () -> vdc("t", "b", "div"));
        System.out.println("Passed the division test...");
    }

    @Test
    void minimumBetweenTwoNumbers() {
        System.out.println("Testing minimum ('min 6 2')...");
        assertEquals(2.0, calculator.calculate(6.0, 2.0, "min"), 0.001);
        System.out.println("Passed the minimum test...");
    }

    @Test
    void minTwoInvalidNumbers() {
        System.out.println("Testing minimum ('min t b')...");
        assertThrows(NumberFormatException.class, () -> vdc("t", "b", "add"));
        System.out.println("Passed the minimum test...");
    }

    @Test
    void maximumBetweenTwoNumbers() {
        System.out.println("Testing maximum ('max 6 2')...");
        assertEquals(6.0, calculator.calculate(6.0, 2.0, "max"), 0.001);
        System.out.println("Passed the maximum test...");
    }

    @Test
    void maxTwoInvalidNumbers() {
        System.out.println("Testing maximum ('max t b')...");
        assertThrows(NumberFormatException.class, () -> vdc("t", "b", "max"));
        System.out.println("Passed the maximum test...");
    }

    @Test
    void squareRootTest() {
        System.out.println("Testing square root ('sqrt 144')...");
        assertEquals(12.0, calculator.calculate(144.0, 0.0, "sqrt"), 0.001);
        System.out.println("Passed the square root test...");
    }

    @Test
    void sqrtTwoInvalidNumbers() {
        System.out.println("Testing sqrt ('sqrt t')...");
        assertThrows(NumberFormatException.class, () -> {
            vdc("t", "b", "sqrt");
        });
        System.out.println("Passed the sqrt test...");
    }

    @Test
    void divisionByZero() {
        System.out.println("Testing division by zero ('div 6 0')...");
        assertThrows(ArithmeticException.class, () -> {
            calculator.calculate(6.0, 0.0, "div");
        });
        System.out.println("Passed the division by zero test...");
    }

    @Test
    void negativeSquareRoot() {
        System.out.println("Testing negative square root ('sqrt -144')...");
        assertThrows(ArithmeticException.class, () -> {
            calculator.calculate(-144.0, 0.0, "sqrt");
        });
        System.out.println("Passed the negative square root test...");
    }

    @Test
    void invalidSquareRoot() {
        System.out.println("Testing invalid square root ('sqrt 144 155')...");
        String message = "Please input one operand.";
        assertEquals(12.0, calculator.calculate(144.0, 155.0, "sqrt"), message);
        System.out.println("Passed the negative square root test...");
    }
}
