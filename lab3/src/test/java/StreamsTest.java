import com.example.streams.BigDecimalOperations;
import com.example.streams.ObjectOperations;
import com.example.streams.PrimitiveOperations;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class StreamsTest {
    @Test
    void sumBigDecimals(){
        BigDecimalOperations bigDecimalOperations = new BigDecimalOperations();
        List<BigDecimal> numbers = Arrays.asList(BigDecimal.ONE, BigDecimal.TEN, BigDecimal.valueOf(5));
        BigDecimal result = bigDecimalOperations.sum(numbers);
        Assertions.assertEquals(BigDecimal.valueOf(16), result);
    }

    @Test
    void averageBigDecimals(){
        BigDecimalOperations bigDecimalOperations = new BigDecimalOperations();
        List<BigDecimal> numbers = Arrays.asList(BigDecimal.ONE, BigDecimal.valueOf(12), BigDecimal.valueOf(5));
        BigDecimal result = bigDecimalOperations.average(numbers);
        Assertions.assertEquals(BigDecimal.valueOf(6), result);
    }

    @Test
    void topTenBigDecimals(){
        BigDecimalOperations bigDecimalOperations = new BigDecimalOperations();
        List<BigDecimal> numbers = Arrays.asList(
                BigDecimal.valueOf(10),
                BigDecimal.valueOf(5),
                BigDecimal.valueOf(8),
                BigDecimal.valueOf(15),
                BigDecimal.valueOf(3),
                BigDecimal.valueOf(12),
                BigDecimal.valueOf(7),
                BigDecimal.valueOf(20),
                BigDecimal.valueOf(1),
                BigDecimal.valueOf(6),
                BigDecimal.valueOf(9)
        );
        List<BigDecimal> result = bigDecimalOperations.topTen(numbers);
        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals(List.of(BigDecimal.valueOf(20)), result);
    }


    @Test
    void sumObject(){
        ObjectOperations objectOperations = new ObjectOperations();
        List<Double> doubles = Arrays.asList(2D, 1D, 15D);
        Double sum = objectOperations.sumDoubleObject(doubles);
        Assertions.assertEquals(18D, sum);
    }

    @Test
    void averageObject(){
        ObjectOperations objectOperations = new ObjectOperations();
        List<Double> doubles = Arrays.asList(2D, 1D, 15D);
        Double sum = objectOperations.averageDoubleObject(doubles);
        Assertions.assertEquals(6D, sum);
    }

    @Test
    void topTenObject(){
        ObjectOperations objectOperations = new ObjectOperations();
        List<Double> doubles = Arrays.asList(10D, 5D, 8D, 15D, 3D, 12D, 7D, 20D, 1D, 6D, 9D);
        List<Double> biggest = objectOperations.topTenDoubleObject(doubles);
        Assertions.assertEquals(1, biggest.size());
        Assertions.assertEquals(List.of(20D), biggest);
    }

    @Test
    void sumPrimitive(){
        PrimitiveOperations primitiveOperations = new PrimitiveOperations();
        double[] doublesArr = {2D, 1D, 15D};
        double res = primitiveOperations.sumDoublePrimitive(doublesArr);
        Assertions.assertEquals(18D, res);
    }

    @Test
    void averagePrimitive(){
        PrimitiveOperations primitiveOperations = new PrimitiveOperations();
        double[] doublesArr = {2D, 1D, 15D};
        double res = primitiveOperations.averageDoublePrimitive(doublesArr);
        Assertions.assertEquals(6D, res);
    }

    @Test
    void topTenPrimitive(){
        PrimitiveOperations primitiveOperations = new PrimitiveOperations();
        double[] doublesArr = {10D, 5D, 8D, 15D, 3D, 12D, 7D, 20D, 1D, 6D, 9D};
        double[] biggestPrimitive = primitiveOperations.topTenDoublePrimitive(doublesArr);
        Assertions.assertEquals(1, biggestPrimitive.length);
        Assertions.assertEquals(20D, biggestPrimitive[0]);
    }
}
