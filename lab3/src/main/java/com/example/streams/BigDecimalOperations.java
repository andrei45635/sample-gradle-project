package com.example.streams;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalOperations {
    public BigDecimal sum(List<BigDecimal> bigDecimals){
        return bigDecimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal average(List<BigDecimal> bigDecimals) {
        return bigDecimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add).divideToIntegralValue(BigDecimal.valueOf(bigDecimals.size()));
    }

    public List<BigDecimal> topTen(List<BigDecimal> bigDecimals){
        return bigDecimals.stream().sorted(Comparator.reverseOrder()).limit((long) (bigDecimals.size() * 0.1)).collect(Collectors.toList());
    }
}
