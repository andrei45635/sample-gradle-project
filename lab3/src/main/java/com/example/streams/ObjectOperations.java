package com.example.streams;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ObjectOperations {
    public Double sumDoubleObject(List<Double> doubles){
        return doubles.stream().reduce(0D, Double::sum);
    }

    public Double averageDoubleObject(List<Double> doubles) {
        return doubles.stream().reduce(0D, Double::sum) / (doubles.size());
    }

    public List<Double> topTenDoubleObject(List<Double> doubles){
        return doubles.stream().sorted(Comparator.reverseOrder()).limit((long) (doubles.size() * 0.1)).collect(Collectors.toList());
    }
}
