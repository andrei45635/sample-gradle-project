package com.example.streams;

import java.util.Arrays;

public class PrimitiveOperations {
    public double sumDoublePrimitive(double[] doubles){
        return Arrays.stream(doubles).sum();
    }

    public double averageDoublePrimitive(double[] doubles){
        return Arrays.stream(doubles).sum() / doubles.length;
    }

    public double[] topTenDoublePrimitive(double[] doubles){
        Arrays.sort(doubles);

        for (int i = 0; i < doubles.length / 2; i++) {
            double temp = doubles[i];
            doubles[i] = doubles[doubles.length - 1 - i];
            doubles[doubles.length - 1 - i] = temp;
        }

        return Arrays.stream(doubles).limit((long) (doubles.length * 0.1)).toArray();
    }
}
