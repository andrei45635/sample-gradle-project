package com.example.jmh;

import com.example.streams.ObjectOperations;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
@State(Scope.Benchmark)
public class DoubleObjectBenchmark {
    @State(Scope.Thread)
    public static class MyState {
        private final ObjectOperations objectOperations = new ObjectOperations();

        @Param({"100000000"})
        public int MAX_COUNT;
        List<Double> doubleList = new ArrayList<>();

        @Setup(Level.Trial)
        public void doSetup() {
            //ascending
//            for (int i = 0; i < MAX_COUNT; i++) {
//                doubleList.add((double) i);
//            }
            //descending
//            for (int i = MAX_COUNT; i > 0; i--) {
//                doubleList.add((double) i);
//            }
           //random
            for (int i = 0; i < MAX_COUNT; i++) {
                doubleList.add(Math.random() * MAX_COUNT);
            }
        }
    }

    @Benchmark
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void sumDoubleObject(Blackhole consumer, MyState state) {
        consumer.consume(state.objectOperations.sumDoubleObject(state.doubleList));
    }

    @Benchmark
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void averageDoubleObject(Blackhole consumer, MyState state) {
        consumer.consume(state.objectOperations.averageDoubleObject(state.doubleList));
    }

    @Benchmark
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void topTenDoubleOject(Blackhole consumer, MyState state) {
        consumer.consume(state.objectOperations.topTenDoubleObject(state.doubleList));
    }
}
