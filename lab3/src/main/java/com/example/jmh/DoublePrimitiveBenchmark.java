package com.example.jmh;

import com.example.streams.PrimitiveOperations;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
@State(Scope.Benchmark)
public class DoublePrimitiveBenchmark {
    @State(Scope.Thread)
    public static class MyState {
        private final PrimitiveOperations primitiveOperations = new PrimitiveOperations();
        @Param({"1000000000"})
        public int MAX_COUNT;
        double[] doubleArray = new double[MAX_COUNT];
        DoubleArrayList doubleArrayList = new DoubleArrayList(doubleArray);

        @Setup(Level.Trial)
        public void doSetup() {
            doubleArrayList.clear();
            //ascending
            for (int i = 0; i < MAX_COUNT; i++) {
                doubleArrayList.add(i);
            }
            //descending
//            for (int i = MAX_COUNT - 1; i >= 0; i--) {
//               doubleArrayList.add(i);
//            }
//          //random
//            for (int i = 0; i < MAX_COUNT; i++) {
//                doubleArrayList.add(Math.random() * MAX_COUNT);
//            }
            doubleArrayList.clear();
        }
    }

    @Benchmark
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void sumDoublePrimitive(Blackhole consumer, MyState state) {
        consumer.consume(state.primitiveOperations.sumDoublePrimitive(state.doubleArray));
    }

    @Benchmark
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void averageDoublePrimitive(Blackhole consumer, MyState state) {
        consumer.consume(state.primitiveOperations.averageDoublePrimitive(state.doubleArray));
    }

    @Benchmark
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void topTenDoublePrimitive(Blackhole consumer, MyState state) {
        consumer.consume(state.primitiveOperations.topTenDoublePrimitive(state.doubleArray));
    }
}
