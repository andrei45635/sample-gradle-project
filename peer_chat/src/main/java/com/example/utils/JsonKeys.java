package com.example.utils;

public enum JsonKeys {
    USERNAME("username"),
    MESSAGE("message");

    public final String label;
    private JsonKeys(String label){
        this.label = label;
    }
}
