package com.example.server;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerThread implements Runnable {
    private Server server;
    private Socket socket;
    private PrintWriter printWriter;
    private ExecutorService executorService;

    public ServerThread(Socket accept, Server server) {
        this.socket = accept;
        this.server = server;
        this.executorService = Executors.newCachedThreadPool();
    }

    public void start() {
        this.executorService.submit(this);
    }

    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            this.printWriter = new PrintWriter(socket.getOutputStream(), true);
            JSONObject jsonObject = new JSONObject()
                    .put("username", server.getUsername())
                    .put("message", "!ack");
            String messageToSend = jsonObject.toString();
            server.sendMessage(messageToSend);
            while (true) {
                server.sendMessage(bufferedReader.readLine());
            }
        } catch (Exception e) {
            System.out.println("Error in run method from ThreadManager: " + e.getMessage());
            server.getServerThreads().remove(this);
        }
    }

    public PrintWriter getPrintWriter() {
        return printWriter;
    }
}
