package com.example.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {
    private String username;
    private ServerSocket socket;
    private Set<ServerThread> serverThreads = new HashSet<>();
    private boolean connected = true;
    private ExecutorService executorService;

    public Server(String username, String port) throws IOException {
        socket = new ServerSocket(Integer.parseInt(port));
        this.username = username;
        executorService = Executors.newCachedThreadPool();
    }

    public void start() {
        executorService.submit(this);
    }

    public void run() {
        try {
            try {
                ServerThread serverThread = new ServerThread(socket.accept(), this);
                serverThreads.add(serverThread);
                serverThread.start();
            } catch (java.net.SocketException e) {
                if (!connected) {
                    socket.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Error in run method from Server: " + e.getMessage());
        }
    }

    public void sendMessage(String messageToSend) {
        try {
            serverThreads.forEach(serverThread -> serverThread.getPrintWriter().println(messageToSend));
        } catch (Exception e) {
            System.out.println("Error in sendMessage method from Server: " + e.getMessage());
        }
    }

    public String getUsername() {
        return username;
    }

    public Set<ServerThread> getServerThreads() {
        return serverThreads;
    }

    public void closeConnectionWithPeer() throws IOException {
        connected = false;
        socket.close();
        System.exit(0);
    }
}
