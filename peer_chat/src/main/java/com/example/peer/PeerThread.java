package com.example.peer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PeerThread implements Runnable {
    private Socket socket;
    private Peer peer;
    private final BufferedReader bufferedReader;
    private boolean ACK = false;
    private ExecutorService executorService;

    public PeerThread(Socket socket, Peer peer) throws IOException {
        this.socket = socket;
        this.peer = peer;
        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        executorService = Executors.newCachedThreadPool();
    }

    public void start() {
        executorService.submit(this);
    }

    public void run() {
        boolean connected = true;
        while (connected) {
            try {
                JSONObject jsonObject = new JSONObject(bufferedReader.readLine());
                if (jsonObject.has("username")) {
                    if (jsonObject.getString("message").equals("!ack")) {
                        ACK = true;
                    }
                    if (ACK) {
                        if (!jsonObject.getString("message").contains("!ack")) {
                            System.out.println("[" + jsonObject.getString("username") + "]:" + jsonObject.getString("message"));
                        } else if (jsonObject.getString("message").contains("!ack")) {
                            ACK = true;
                            System.out.println("[" + jsonObject.getString("username") + "]:" + jsonObject.getString("message"));
                        }
                    } else {
                        System.out.println("Couldn't ACK-! on " + socket.getPort());
                        peer.getConnections().remove(String.valueOf(socket.getPort()));
                        connected = false;
                    }
                } else {
                    System.out.println("Something went wrong. Invalid JSON format maybe?");
                }
            } catch (JSONException | IOException je) {
                System.out.println("Error in run method from PeerThread: " + je.getMessage());
                peer.getConnections().remove(String.valueOf(socket.getPort()));
                connected = false;
            }
        }
    }
}
