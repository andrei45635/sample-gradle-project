package com.example.peer;

import com.example.server.Server;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Peer {
    private static String username;
    private static String port;
    private static Server server;
    private Map<String, PeerThread> connections = new HashMap<>();

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Input your username and port, separated with a space");
        String[] credentials = bufferedReader.readLine().split(" ");
        username = credentials[0];
        port = credentials[1];
        server = new Server(username, port);
        server.start();
        new Peer().startup(bufferedReader);
    }

    public Map<String, PeerThread> getConnections() {
        return connections;
    }

    private void startup(BufferedReader bufferedReader) {
        System.out.println("===COMMAND LIST===\n !hello <port> - create connection\n !bye <port> - close connection\n !byebye - close all connections and exit\n");
        chat(bufferedReader);
    }

    private void chat(BufferedReader bufferedReader) {
        try {
            label:
            while (true) {
                String message = bufferedReader.readLine();
                String command = parseCommand(message);
                switch (command) {
                    case "byebye":
                        createSendMessage(server, "bcast", "User " + username + " has ended the connection.");
                        server.closeConnectionWithPeer();
                        break label;
                    case "bye":
                        server.closeConnectionWithPeer();
                        break;
                    case "null":
                        createSendMessage(server, username, message);
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("Error in chat: " + e.getMessage());
            System.exit(0);
        }
    }

    private void createSendMessage(Server server, String username, String message) {
        JSONObject jsonObject = new JSONObject()
                .put("username", username)
                .put("message", message);
        String messageToSend = jsonObject.toString();
        server.sendMessage(messageToSend);
    }

    private String parseCommand(String message) {
        String[] parts = message.split(" ");
        switch (parts[0]) {
            case "!hello":
                try {
                    initalizeSocket(parts[1]);
                } catch (Exception e) {
                    System.out.println("Error when trying to initialize socket in Peer: " + e.getMessage());
                }
                return "hello";
            case "!byebye":
                return "byebye";
            case "!bye":
                return "bye";
            default:
                return "null";
        }
    }

    private void initalizeSocket(String port) throws IOException {
        Socket socket = null;
        try {
            if (!connections.containsKey(port)) {
                System.out.println("Trying to connect to port:" + port + "...");
                socket = new Socket("localhost", Integer.parseInt(port));
                PeerThread peerThread = new PeerThread(socket, this);
                connections.put(port, peerThread);
                peerThread.start();
            } else {
                System.out.println("Already connected to port:" + port);
            }
        } catch (Exception e) {
            if (socket != null) {
                socket.close();
            }
            System.out.println("Couldn't connect, error: " + e.getMessage());
        }
    }
}
