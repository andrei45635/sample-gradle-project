package com.example.repos;

import it.unimi.dsi.fastutil.objects.ObjectArraySet;

public class ObjectArraySetBasedRepository<T> implements InMemoryRepository<T> {
    private final ObjectArraySet<T> objectArraySet;

    public ObjectArraySetBasedRepository() {
        this.objectArraySet = new ObjectArraySet<>();
    }

    @Override
    public void add(T elem) {
        objectArraySet.add(elem);
    }

    @Override
    public void remove(T elem) {
        objectArraySet.remove(elem);
    }

    @Override
    public boolean contains(T elem) {
        return objectArraySet.contains(elem);
    }

    @Override
    public void clear(){
        objectArraySet.clear();
    }
}
