package com.example.repos;

public interface InMemoryRepository<T> {
    void add(T elem);

    void remove(T elem);

    boolean contains(T elem);

    void clear();
}
