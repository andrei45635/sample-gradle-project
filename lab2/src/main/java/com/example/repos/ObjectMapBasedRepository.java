package com.example.repos;

import com.koloboke.collect.map.hash.HashObjObjMap;
import com.koloboke.collect.map.hash.HashObjObjMaps;

public class ObjectMapBasedRepository<T> implements InMemoryRepository<T> {
    private final HashObjObjMap<T, String> hashObjObjMap;

    public ObjectMapBasedRepository() {
        this.hashObjObjMap = HashObjObjMaps.newMutableMap();
    }

    @Override
    public void add(T elem) {
        hashObjObjMap.put(elem, "elem");
    }

    @Override
    public void remove(T elem) {
        hashObjObjMap.remove(elem);
    }

    @Override
    public boolean contains(T elem) {
        return hashObjObjMap.containsKey(elem);
    }

    @Override
    public void clear(){
        hashObjObjMap.clear();
    }
}
