package com.example.repos;

import gnu.trove.set.hash.THashSet;

public class THashSetBasedRepository<T> implements InMemoryRepository<T> {
    private final THashSet<T> hashSet;

    public THashSetBasedRepository() {
        this.hashSet = new THashSet<>();
    }

    @Override
    public void add(T elem) {
        hashSet.add(elem);
    }

    @Override
    public void remove(T elem) {
        hashSet.remove(elem);
    }

    @Override
    public boolean contains(T elem) {
        return hashSet.contains(elem);
    }

    @Override
    public void clear(){
        hashSet.clear();
    }
}
