package com.example.repos;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private final ConcurrentHashMap<T, String> concurrentHashMap;

    public ConcurrentHashMapBasedRepository() {
        this.concurrentHashMap = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T elem) {
        concurrentHashMap.put(elem, "elem");
    }

    @Override
    public void remove(T elem) {
        concurrentHashMap.remove(elem);
    }

    @Override
    public boolean contains(T elem) {
        return concurrentHashMap.containsKey(elem);
    }

    @Override
    public void clear() {
        concurrentHashMap.clear();
    }
}
