package com.example.repos;

import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

public class MutableListBasedRepository<T> implements InMemoryRepository<T> {
    private final MutableList<T> mutableList;

    public MutableListBasedRepository() {
        this.mutableList = Lists.mutable.empty();
    }

    @Override
    public void add(T elem) {
        mutableList.add(elem);
    }

    @Override
    public void remove(T elem) {
        mutableList.remove(elem);
    }

    @Override
    public boolean contains(T elem) {
        return mutableList.contains(elem);
    }

    @Override
    public void clear(){
        mutableList.clear();
    }
}
