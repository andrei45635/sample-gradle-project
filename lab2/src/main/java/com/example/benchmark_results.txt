Benchmark                                       Mode   Cnt   Score         Error    Units
ArrayListBenchmark.testMethodAdd               thrpt   20    11896.507 �   351.346  ops/ms
ArrayListBenchmark.testMethodContains          thrpt   20   933056.105 � 19620.989  ops/ms
ArrayListBenchmark.testMethodRemove            thrpt   20   939836.836 � 25029.806  ops/ms
ConcurrentHashMapBenchmark.testMethodAdd       thrpt   20     3978.596 �    76.173  ops/ms
ConcurrentHashMapBenchmark.testMethodContains  thrpt   20     7975.906 �    62.774  ops/ms
ConcurrentHashMapBenchmark.testMethodRemove    thrpt   20     7325.078 �    73.908  ops/ms
HashObjectMapBenchmark.testMethodAdd           thrpt   20     4576.152 �    36.668  ops/ms
HashObjectMapBenchmark.testMethodContains      thrpt   20     5292.706 �   100.427  ops/ms
HashObjectMapBenchmark.testMethodRemove  	   thrpt   20  	  3583.882 �   131.403  ops/ms
HashSetBenchmark.testMethodAdd                 thrpt   20     5707.767 �   158.921  ops/ms
HashSetBenchmark.testMethodContains            thrpt   20     7729.802 �   114.715  ops/ms
HashSetBenchmark.testMethodRemove              thrpt   20     8003.176 �    49.907  ops/ms
MutableListBenchmark.testMethodAdd             thrpt   20    12408.113 �   146.009  ops/ms
MutableListBenchmark.testMethodContains        thrpt   20  1055562.356 � 12821.525  ops/ms
MutableListBenchmark.testMethodRemove          thrpt   20  1053918.498 � 17803.305  ops/ms
ObjectArrayBenchmark.testMethodAdd             thrpt   20    67897.875 �   531.954  ops/ms
ObjectArrayBenchmark.testMethodContains        thrpt   20   112975.083 �  2338.231  ops/ms
ObjectArrayBenchmark.testMethodRemove          thrpt   20   114324.524 �  1517.233  ops/ms
THashSetBenchmark.testMethodAdd                thrpt   20     6907.206 �   134.008  ops/ms
THashSetBenchmark.testMethodContains           thrpt   20     7004.928 �   125.936  ops/ms
THashSetBenchmark.testMethodRemove             thrpt   20     7036.983 �    34.930  ops/ms
TreeSetBenchmark.testMethodAdd                 thrpt   20    31981.664 �  1182.442  ops/ms
TreeSetBenchmark.testMethodContains            thrpt   20   629089.583 � 14115.275  ops/ms
TreeSetBenchmark.testMethodRemove              thrpt   20   645651.215 � 19835.846  ops/ms