package com.example.jmh;


import com.example.domain.Order;
import com.example.repos.ConcurrentHashMapBasedRepository;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
@State(Scope.Benchmark)
public class ConcurrentHashMapBenchmark {
    @State(Scope.Thread)
    public static class MyState {
        public ConcurrentHashMapBasedRepository<Order> concurrentHashMapBasedRepository = new ConcurrentHashMapBasedRepository<>();
        public int iterations = 10;
        Order order = new Order(0, 999, 99);

        @Setup(Level.Iteration)
        public void doSetup() {
            for (int i = 0; i < iterations; i++) {
                concurrentHashMapBasedRepository.add(order);
            }
        }

        @TearDown(Level.Iteration)
        public void doTeardown(){
            concurrentHashMapBasedRepository.clear();
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void testMethodAdd(MyState state) {
        for(int i = 0; i < state.iterations; i++){
            state.concurrentHashMapBasedRepository.add(state.order);
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void testMethodRemove(MyState state) {
        for(int i = 0; i < state.iterations; i++){
            state.concurrentHashMapBasedRepository.remove(state.order);
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void testMethodContains(MyState state) {
        for(int i = 0; i < state.iterations; i++){
            state.concurrentHashMapBasedRepository.contains(state.order);
        }
    }
}
