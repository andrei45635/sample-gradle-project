package com.example.jmh;


import com.example.domain.Order;
import com.example.repos.ArrayListBasedRepository;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1)
@Measurement(iterations = 5, time = 1)
@Fork(1)
@State(Scope.Benchmark)
public class ArrayListBenchmark {

    @State(Scope.Thread)
    public static class MyState {
        public ArrayListBasedRepository<Order> arrayListBasedRepository = new ArrayListBasedRepository<>();
        public int iterations = 10;
        Order order = new Order(0, 999, 99);

        @Setup(Level.Iteration)
        public void doSetup() {
            for (int i = 0; i < iterations; i++) {
                arrayListBasedRepository.add(order);
            }
        }

        @TearDown(Level.Iteration)
        public void doTeardown() {
            arrayListBasedRepository.clear();
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void testMethodAdd(MyState state) {
        for (int i = 0; i < state.iterations; i++) {
            state.arrayListBasedRepository.add(state.order);
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void testMethodRemove(MyState state) {
        for (int i = 0; i < state.iterations; i++) {
            state.arrayListBasedRepository.remove(state.order);
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void testMethodContains(MyState state) {
        for (int i = 0; i < state.iterations; i++) {
            state.arrayListBasedRepository.contains(state.order);
        }
    }
}
